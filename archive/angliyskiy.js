(function($) {
window.onload = function(){
	if($("div").hasClass("slider")) {
		var $set = $('.button-slider');
		var n=1;
		var d=3000;
		setInterval(function(){
  			$('.buttons-slider span').eq(n).trigger('click');
  			n++;
  			if (n<=9 && n>=6) {
  				$(".kurs").velocity({'opacity': '1'},{duration: 1000})
  			} else {
  				$(".kurs").velocity({'opacity': '0'},{duration: 0})
  			}
  			if (n==8) n=0;
		}, 7000);
		$('.buttons-slider').on('click','span', function () {
			if(!$(this).hasClass("active")) {
				d=7000;
				n=$set.index(this); 
				$('.button-slider').removeClass("active");
				$(this).addClass("active");   
				
				var image_prev=$("div.active");
				var image=$(".slider-content div").eq(n);
				var desc_prev=$(".description.active");
				var desc=$(".description").eq(n);
				if (n<=13 && n>=9) {
	  				$(".kurs").velocity({'opacity': '1'},{duration: 1000})
	  			} else {
	  				$(".kurs").velocity({'opacity': '0'},{duration: 0})
	  			}
				if(image.hasClass("animation-top")){
					animationSliderTop(image, image_prev, desc, desc_prev);
				}
				if(image.hasClass("animation-left")){
					animationSliderLeft(image, image_prev, desc, desc_prev);
				}
				if(image.hasClass("animation-opacity")){
					animationSliderOpacity(image, image_prev, desc, desc_prev);
				}
				
			}
			
		});
	}
	if(jQuery("img").hasClass("trans2")) {
		jQuery(".trans2").animate({"margin-top":'0', "margin-left": '0', opacity: '1'},1000); 
	}
	if(jQuery("div").is("#rotator")) {
		theRotator();	
	}
	if(jQuery("div").hasClass("trans13")) {
		jQuery(".trans13").animate({opacity: '1'}, 500); 
	}
	if(jQuery("div").hasClass("trans14")) {
		jQuery(".trans14").delay(500).animate({opacity: '0', "margin": '-85px 0 0 500px'}, 500);
	}
	if(jQuery("div").hasClass("trans15")) {
		jQuery(".trans15").delay(1000).animate({opacity: '0', "margin": '-85px 0 0 500px'}, 500);
	}
	if(jQuery("div").hasClass("trans16")) {
		jQuery(".trans16").delay(1500).animate({opacity: '0', "margin": '-85px 0 0 500px'}, 500);
	}
	if(jQuery("div").hasClass("trans17")) {
		jQuery(".trans17").delay(2000).animate({opacity: '0', "margin": '-76px 0 0 500px'}, 500);
	}
	if(jQuery("div").hasClass("trans18")) {
		jQuery(".trans18").delay(2500).animate({opacity: '0', "margin": '-85px 0 0 500px'}, 500);
	}
	if(jQuery("div").hasClass("trans19")) {
		jQuery(".trans19").delay(3000).animate({opacity: '0', "margin": '-85px 0 0 500px'}, 500);
	}
	if(jQuery("div").hasClass("trans20")) {
		jQuery(".trans20").delay(3500).animate({opacity: '0', "margin": '-85px 0 0 500px'}, 500);
	}
	if($("div").hasClass("trans21"))  {
		if($.browser.msie && $.browser.version <= 8 ) {
			$(".trans23").css("opacity", "1");	
			$(".trans21").css("display", "none");
			$(".trans22").css("display", "none");
		} else {
			$.preloadImages("http://verovenko.ru/wp-content/themes/twentyfourteen/images/buran2.jpg");
			$(".trans23").css("opacity", "1");
			$(".trans21").delay(1400).velocity({"margin-top": '-100px', "margin-left": '650px'}, 800);
			$(".trans22").delay(2200).velocity({"margin-left": '750px'}, 900);
			$(".trans24").velocity({ rotateZ: "-90deg", "margin-top": '-400px', "margin-left":'-450px' },0);
			$(".trans24").delay(3100).velocity({"margin-top": '0', "margin-left":'0', "margin-bottom":'0', "margin-right":'0', "opacity": '1', rotateZ: "0deg"},{ duration:1200, easing:"swing"});
			$(".trans25").delay(4300).velocity({"opacity": '1'},1200);
			$(".trans26").delay(5500).velocity({scale: 0.8},0)
			.velocity({"opacity": '1', scale: 1},2500);
			$('.cube').velocity({ rotateY: "180deg", scale: 0.2, translateX: -1000, "margin-left":'-240px' }, 0)
			.velocity({ rotateY: "270deg", scale: 0.6, translateX: 0, "opacity": '0.5' },{ duration:600, easing:'linear'})
			.velocity({ rotateY: "360deg", translateX: 240, scale: 1, "opacity": '1' },{ duration:600, easing:'linear'});
			$('.cube1').delay(1300).velocity({ "opacity": '1' }, 500);
		}
	}
	if(jQuery("div").is("#rotateimg3")) {
		jQuery("#hide").fadeIn(500);
		jQuery("#rotateimg3").rotate({animateTo:1440, duration: 4500});
		var hoverHeight=341;
	    hoverWidth=340;
	    jQuery("#rotateimg3").animate({height:hoverHeight, width:hoverWidth},1000);
	}
	/*document.ondragstart = noselect; 
    // запрет на перетаскивание 
    document.onselectstart = noselect; 
    // запрет на выделение элементов страницы 
    document.oncontextmenu = noselect; 
    // запрет на выведение контекстного меню 
    function noselect() {return false;}*/
}
})(jQuery);

jQuery(window).scroll(function() {
	if(jQuery("div").is("#rotateimg33")) {
		if (come("#rotateimg33")) {
	        jQuery("#hide").fadeIn(500);
			jQuery("#rotateimg33").rotate({animateTo:1440, duration: 4500});
			var hoverHeight=341;
		    hoverWidth=340;
		    jQuery("#rotateimg33").animate({height:hoverHeight, width:hoverWidth},1000);
		}
	}
	if(jQuery("div").hasClass("trans")) {
		if (come(".trans")) {
	        jQuery(".trans1").animate({"margin-left": '0', "opacity": '1'},1000);
		}
	}
	if(jQuery("div").hasClass("trans")) {
		if (come(".trans")) {
	        jQuery(".trans3").animate({"margin-top":'0', "margin-left": '0', "opacity": '1'},1000);
		}
	}
	if(jQuery("div").is("#hide1")) {
		if (come("#hide1")) {
			jQuery("#hide1").animate({"opacity": '1'},3000);
		}
	}
	if(jQuery("div").is("#rotateimg")) {
		if (come("#rotateimg")) {
			jQuery("#hide").fadeIn(500);
			jQuery("#rotateimg").rotate({animateTo:720, duration: 3000});
			var hoverHeight=439;
	        hoverWidth=438;
	        jQuery("#rotateimg").animate({height:hoverHeight, width:hoverWidth},1000);
		}
	}
	if(jQuery("div").is("#rotateimg1")) {
		if (come("#rotateimg1")) {
			jQuery("#rotateimg1").rotate({animateTo:720, duration: 3000});
			var hoverHeight=450;
	        hoverWidth=339;
	        jQuery("#rotateimg1").animate({height:hoverHeight, width:hoverWidth, opacity: '1'},1000);
		}
	}
	if(jQuery("div").is("#rotateimg4")) {
		if (come("#rotateimg4")) {
			jQuery("#rotateimg4").rotate({animateTo:1080, duration: 3000});
			var hoverHeight=460;
	        hoverWidth=304;
	        jQuery("#rotateimg4").animate({height:hoverHeight, width:hoverWidth, opacity: '1'},1000);
		}
	}
});
function come(elem) {
	var docViewTop = jQuery(window).scrollTop(),
		docViewBottom = docViewTop + jQuery(window).height() + jQuery(elem).height() * 0.5,
		elemTop = jQuery(elem).offset().top,
		elemBottom = elemTop + jQuery(elem).height();

		if ((elemBottom <= docViewBottom) && (docViewTop <= elemTop)) {			
			return (true);
		}
	
}
function theRotator() {
	// Устанавливаем прозрачность всех картинок в 0
	jQuery('div#rotator ul li').css({opacity: 0.0});
 
	// Берем первую картинку и показываем ее (по пути включаем полную видимость)
	jQuery('div#rotator ul li:first').css({opacity: 1.0});
 
	// Вызываем функцию rotate для запуска слайдшоу, 5000 = смена картинок происходит раз в 5 секунд
	setInterval('rotate()',5000);
}
 function rotate() {	
	// Берем первую картинку
	var current = (jQuery('div#rotator ul li.show')?  jQuery('div#rotator ul li.show') : jQuery('div#rotator ul li:first'));
 
	// Берем следующую картинку, когда дойдем до последней начинаем с начала
	var next = ((current.next().length) ? ((current.next().hasClass('show')) ? jQuery('div#rotator ul li:first') :current.next()) : jQuery('div#rotator ul li:first'));	
 
	// Расскомментируйте, чтобы показвать картинки в случайном порядке
	// var sibs = current.siblings();
	// var rndNum = Math.floor(Math.random() * sibs.length );
	// var next = $( sibs[ rndNum ] );
 
	// Подключаем эффект растворения/затухания для показа картинок, css-класс show имеет больший z-index
	next.css({opacity: 0.0})
	.addClass('show')
	.animate({opacity: 1.0}, 1000);
 
	// Прячем текущую картинку
	current.animate({opacity: 0.0}, 1000)
	.removeClass('show');
}
jQuery.preloadImages = function()
{
 for(var i = 0; i<arguments.length; i++)
 {
  jQuery("<img>").attr("src", arguments[i]);
 }
}
function animationSliderTop(elem, elem_prev, desc1, desc_prev1) {
	
	if(jQuery(elem_prev).hasClass("animation-top")) {
		jQuery(elem_prev).velocity({'margin-top': '706px'},{ duration: 1000, queue: false}).removeClass('active'); 
		if(jQuery(desc_prev1).hasClass("description")) {
			jQuery(desc_prev1).velocity({'margin-top': '706px'},{duration: 1000}).removeClass('active');
			jQuery(desc_prev1).velocity({'top': '720px', 'margin-top': '0', 'margin-left': '0'},{duration: 0});
		} 
		jQuery(elem_prev).velocity({'margin-top': '-706px', 'z-index': '1'}, 0)
	}
	if(jQuery(elem_prev).hasClass("animation-left")) {
	
		jQuery(elem_prev).velocity({'margin-top': '706px'},{ duration: 1000, queue: false}).removeClass('active');
		
		if(jQuery(desc_prev1).hasClass("description")) {
			jQuery(desc_prev1).velocity({'margin-top': '706px'},{duration: 1000}).removeClass('active');
			jQuery(desc_prev1).velocity({'top': '720px', 'margin-top': '0', 'margin-left': '0'},{duration: 0});
		}
		jQuery(elem_prev).velocity({'margin-left': '1279px', 'margin-top': '0', 'z-index': '1'}, 0)
	} 
	if(jQuery(elem_prev).hasClass("animation-opacity")) {
		jQuery(elem_prev).velocity({'margin-top': '706px'}, { duration: 1000, queue: false}).removeClass('active');
		if(jQuery(desc_prev1).hasClass("description")) {
			jQuery(desc_prev1).velocity({'opacity': '0'},{duration: 1000}).removeClass('active');
			jQuery(desc_prev1).velocity({'top': '720px', 'margin-top': '0', 'opacity': '1'},{duration: 0});
		}
		jQuery(elem_prev).velocity({'z-index': '1', 'opacity': '0', 'margin-top': '0'}, 0)
	}
	if(jQuery(elem_prev).hasClass("animation-opacity-zoom")) {
		jQuery(elem_prev).velocity({scale: 1}, { duration: 0, queue: false});
		if(jQuery(desc_prev1).hasClass("description")) {
			jQuery(desc_prev1).velocity({'top': '720px', 'margin-top': '0', 'opacity': '1'},{duration: 0}).removeClass('active');
		}
	}
	if(jQuery(desc1).hasClass("description")) {
		jQuery(desc1).css({'z-index': '3', 'margin': '0', 'top': '720px', 'opacity': '1'}).velocity({'top': '270px'},{ duration: 1000, delay: 1000}).addClass('active');
	}
	if(jQuery(elem).hasClass("animation-top")) {
		jQuery(elem).css('z-index', '2').velocity({'margin-top': '0'}, { duration: 1000}).addClass('active');
	}
}
function animationSliderLeft(elem, elem_prev, desc1, desc_prev1) {
	if(jQuery(elem).hasClass("animation-left")) {
		jQuery(elem).css('z-index', '2').velocity({'margin-left': '0'}, 1000).addClass('active');

	}
	if(jQuery(elem_prev).hasClass("animation-top")) {
		jQuery(elem_prev).velocity({'margin-left': '-1279px'},{ duration: 1000, queue: false}).removeClass('active');
		if(jQuery(desc_prev1).hasClass("description")) {
			jQuery(desc_prev1).velocity({'margin-left': '-1279px'},{duration: 1000}).removeClass('active');
			jQuery(desc_prev1).velocity({'top': '720px', 'margin-top': '0', 'margin-left': '0'}, 0);
		}
		jQuery(elem_prev).velocity({'margin-left': '0', 'margin-top': '-706px', 'z-index': '1'}, 0);
	} 
	if(jQuery(elem_prev).hasClass("animation-left")) {
		jQuery(elem_prev).velocity({'margin-left': '-1279px'}, { duration: 1000});
		if(jQuery(desc_prev1).hasClass("description")) {
			jQuery(desc_prev1).velocity({'margin-left': '-1279px'},{duration: 1000}).removeClass('active');
			jQuery(desc_prev1).velocity({'top': '720px', 'margin-top': '0', 'margin-left': '0'}, 0);
		}
		jQuery(elem_prev).velocity({"margin-left": "1279px",'z-index': '1'},0);
	} 
	if(jQuery(elem_prev).hasClass("animation-opacity")) {
		jQuery(elem_prev).velocity({'margin-left': '-1279px'}, { duration: 1000, queue: false}).removeClass('active');
		if(jQuery(desc_prev1).hasClass("description")) {
			jQuery(desc_prev1).velocity({'margin-left': '-1279px'},{duration: 1000}).removeClass('active');
			jQuery(desc_prev1).velocity({'top': '720px', 'margin-top': '0', 'margin-left': '0','opacity': '1'}, 0);
		}
		jQuery(elem_prev).velocity({'z-index': '1', 'opacity': '0', 'margin-left': '0'}, 0);
	} 
	if(jQuery(elem_prev).hasClass("animation-opacity-zoom")) {
		jQuery(elem_prev).velocity({scale: 1}, { duration: 0, queue: false});
		if(jQuery(desc_prev1).hasClass("description")) {
			jQuery(desc_prev1).velocity({'top': '720px', 'margin-top': '0', 'margin-left': '0'}, 0);
		}
	}
	if(jQuery(desc1).hasClass("description")) {
		jQuery(desc1).css({'z-index': '3', 'margin': '0', 'opacity': '1'}).velocity({'top': '270px'},{ duration: 1000, delay: 1000}).addClass('active');
	}
}
function animationSliderOpacity(elem, elem_prev, desc1, desc_prev1) {	
	if(jQuery(elem).hasClass("animation-opacity")) {
		if(jQuery(elem).hasClass("animation-opacity-zoom")) {
			jQuery(elem).css({'z-index': '3'}).velocity({'opacity': '1'}, 1000).addClass('active');
			jQuery(elem).velocity({scale: 1.1}, {duration: 3000, queue: false});
		} else {
			jQuery(elem).css('z-index', '3').velocity({'opacity': '1'}, 1000).addClass('active');
		}
	}
	if(jQuery(elem_prev).hasClass("animation-opacity-zoom")) {
		jQuery(elem_prev).css('z-index', '2').removeClass('active');
		if(jQuery(desc_prev1).hasClass("description")) {
			jQuery(desc_prev1).velocity({'opacity': '0'},{ duration: 1000}).removeClass('active');
			jQuery(desc_prev1).velocity({'top': '720px','opacity': '1'},{ duration: 0});
		} 
		jQuery(elem_prev).velocity({scale: 1, 'opacity': '0'}, 0);
	} else {
		if(jQuery(elem_prev).hasClass("animation-opacity")) {
			jQuery(elem_prev).css('z-index', '2').removeClass('active');
			if(jQuery(desc_prev1).hasClass("description")) {
				jQuery(desc_prev1).velocity({'opacity': '0'},{ duration: 1000}).removeClass('active');
				jQuery(desc_prev1).velocity({'top': '720px','opacity': '1'},{ duration: 0});
			} 
			jQuery(elem_prev).velocity({'opacity': '0', 'margin-left': '0'},0)
		}
	} 
	if(jQuery(elem_prev).hasClass("animation-top")) {	
		if(jQuery(desc_prev1).hasClass("description")) {
			jQuery(desc_prev1).velocity({'opacity': '0'},{ duration: 1000}).removeClass('active');
			jQuery(desc_prev1).velocity({'top': '720px','opacity': '1','margin-left': '0'},0);
		} 
		jQuery(elem_prev).velocity({'margin-left': '0', 'margin-top': '-706px', 'z-index': '1'}, { duration: 0}).removeClass('active');
	} 
	if(jQuery(elem_prev).hasClass("animation-left")) {
		
		if(jQuery(desc_prev1).hasClass("description")) {
			jQuery(desc_prev1).velocity({'opacity': '0'},{ duration: 1000}).removeClass('active');
			jQuery(desc_prev1).velocity({'top': '720px','opacity': '1','margin-left': '0'},{ duration: 0, delay: 1000});
		}
		jQuery(elem_prev).velocity({'margin-left': '1279px', 'z-index': '1'}, { duration: 0, delay: 1000}).removeClass('active');
	}	
	if(jQuery(desc1).hasClass("description")) {
		jQuery(desc1).css({'z-index': '3', 'top': '720px', 'margin': '0', 'opacity': '1'}).velocity({'top': '270px'},{ duration: 1000, delay: 1000}).addClass('active');
	}
}


